# Simple client for OWM api.
# Author: Samuel Mensak, 6.3.2019
# Made as a project for IPK at FIT BUT
# All code below was written by author.
# For more info about socket programming see: https://docs.python.org/3/library/socket.html

import socket
import sys
import json

# !!!WARNING!!!
# If program is executed without makefile, arguments are not guarded!
# Expected make arguments: api_key=<___> city=<___>
# Expected script arguments: <api_key> <city>

api_key = sys.argv[1]
city = sys.argv[2]

port = 80 # standard HTTP port
host = 'api.openweathermap.org' # API server address
api_path = '/data/2.5/' # API path
weather_type = 'weather' # weather(current), forecast, etc.
units = '&units=' + 'metric' # units: metric or imperial 
api_key = '&APPID=' + api_key
request = "?q=" + city + units + api_key 
message = ( "GET " + api_path + weather_type + request + " HTTP/1.1\r\n" # HTTP message / request
			"Host: " + host + "\r\n"
			"Connection: close\r\n\r\n")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # create socket
try:
	s.settimeout(10) # limit time for connection atempt to 10s.
	s.connect((host, port)) # try to connect to API server

except socket.timeout: # could not connect in 10s
	s.close()
	sys.exit('Connection timed out (10s).')

except socket.gaierror: # conecction error
	s.close()
	sys.exit('Could not connect to OWM server.')

except InterruptedError: # if connection in interupted
	s.close()
	sys.exit('Connection intreupted.')

s.settimeout(None) # no time limit for sending request

try:
	s.sendall(message.encode()) # send request
except InterruptedError:
	s.close()
	sys.exit('Could not send all data, connection intreupted.')

try:
	s.settimeout(10) # time to wait for response
	response = s.recv(4096) # recieve response from API
except socket.timeout:
	s.close()
	sys.exit('Connection timed out. Did not recieve response from server in 10s.')

s.settimeout(None)
s.close() # data recieved, close the socket

try: 
	head, data = response.split(b'\r\n\r\n', 1) # separate HTTP head from data
except ValueError:
	sys.exit('Invalid HTTP response from server.')

try:
	head = head.decode().splitlines()[0] # get the first line of HTTP response
	etc, code, message = head.split(' ', 2) # parse it 
except ValueError:
	sys.exit('Failed to parse HTTP response from server.')

if (etc != 'HTTP/1.1'):
	sys.exit('Invalid HTTP response from server.')

if (code != '200'): # if HTTP return code indicates unsuccessful request
	sys.exit('Unable to get weather data. Server returned code: ' + code + " - " + message + '.')

try:
	data = json.loads(data.decode()) # get JSON from response data
except ValueError:
	sys.exit('Failed to parse data from server.')

# OUTPUT # print weather data for given city

try:
	print(data['name'])
except KeyError:
	print('No city name in response to request for weather in' + city + '.')

try: 
	print(data['weather'][0]['description'])
except (IndexError, KeyError):
	print('No weather description.')

try: 
	print('temp: {} deg. C'.format(data['main']['temp']))
except (KeyError):
	print('temp: No temperature inforamtion available.')

try: 
	print('humidity: {}%'.format(data['main']['humidity']))
except (KeyError):
	print('humidity: No humidity information available.')

try: 
	print('pressure: {}hPa'.format(data['main']['pressure']))
except (KeyError):
	print('pressure: No pressure information available.')

try: 
	print('wind-speed: {}km/h'.format(int(data['wind']['speed'])*3,6))
except (KeyError):
	print('wind-speed: No wind speed information available.')

try: 
	print('wind-deg: {}'.format(data['wind']['deg']))
except (KeyError):
	print('wind-deg: No wind direction information available.')
	
# End ;)
