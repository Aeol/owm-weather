# expected target api_key=<___> city=<___>, anything else will be ignored
run:
ifndef city
	$(error Invalid or missing city argument)
else
ifndef api_key
	$(error Invalid or missing api_key argument)
else
	@python client.py $(api_key) "$(city)"
endif
endif

build:
	@:

%:
	@:
